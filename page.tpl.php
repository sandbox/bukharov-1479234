<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
  <head>
    <?php print $head ?>
    <title>ExtAdmin - <?php print t('Drupal administration')?></title>
    <link type="text/css" rel="stylesheet" href="<?php print base_path() . drupal_get_path('theme', 'extadmin_theme'); ?>/css/Loader.css" />
  </head>
  <body>
    <div id="loading-mask"></div>
    <div id="loading">
	<div class="loading-indicator"><?php print t('Loading ExtAdmin...')?></div>
    </div>
    <?php print $styles ?>
    <?php print $scripts ?>
  </body>
</html>
