<?php
  function extadmin_theme_preprocess_page(&$variables) {
    if (!variable_get('clean_url', FALSE)) {
      // When clean urls are disabled api.js include looks like this: index.php?q=extadmin/api.js?something.
      // That obviously wont work, so we fix that.
      $variables['scripts'] = preg_replace('#(extadmin/api\.js)\?\w+#', '$1', $variables['scripts']);
    }

    if (variable_get('preprocess_js', FALSE)) {
      if (preg_match_all('#(<script.+?src=".+?extadmin.+?"></script>)#i', $variables['scripts'], $matches)) {
        // When preprocessing is enabled combined JS file comes first and then everything else.
        // We need to make sure that api.js and ext-all.js come first.
        $scripts = $variables['scripts'];
        $extadmin = '';
        foreach ($matches[1] as $match) {
          $extadmin .= $match . "\n";
          $scripts = str_replace($match . "\n", '', $scripts);
        }
        $variables['scripts'] = $extadmin.$scripts;
      }
    }
  }
