<?php

  function extadmin_theme_settings($saved_settings) {
    $defaults = array(
      'ext_theme' => 'default'
    );

    $settings = array_merge($defaults, $saved_settings);

    $options = array(
      '' => 'default'
    );

    $path = drupal_get_path('module', 'extadmin')  . '/ext/resources/themes/images';
    foreach (scandir($path) as $file) {
      if (is_dir($path . '/' . $file) && $file != 'default' && $file != '.' && $file != '..') {
        $options[$file] = $file;
      }
    }

    $form['ext_theme'] = array(
      '#type' => 'select',
      '#title' => 'ExtJS Theme',
      '#default_value' => $settings['ext_theme'],
      '#options' => $options
    );

    return $form;
  }
